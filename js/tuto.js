var hasShopId = false
var hasAppshopId = false
var hasUserId = false

const copyToClipboard = (str) => {
  const el = document.createElement('textarea')
  el.value = str
  el.setAttribute('readonly', '')
  el.style.position = 'absolute'
  el.style.left = '-9999px'
  document.body.appendChild(el)
  el.select()
  document.execCommand('copy')
  document.body.removeChild(el)
}

// Catch XHR request
(function () {

  const send = XMLHttpRequest.prototype.send
  XMLHttpRequest.prototype.send = function () {
    this.addEventListener('load', function () {
      let URL = this.responseURL
      console.log('==== Header', URL)
      if (URL.includes('staging-files-bo.beeketing.com') || URL.includes('sdk.beeketing.com/shop')) {
        console.log('== OK. Set click nowww')
        let shopScript = JSON.parse(this.responseText)

        // Get detected platform
        let platform = 'staging'
        let hiveURL = 'https://staging-hive.beeketing.com/admin/view-shop/'
        let platformDOM = document.getElementById('bk_platform')

        if (platformDOM && platformDOM.value == 'production') {
          hiveURL = 'https://hive.beeketing.com/admin/view-shop/'
        }

        // Set hive button
        let shopID = 0
        if (shopScript && shopScript.shop) {
          shopID = shopScript.shop.id
        }

        const setOnclick = function () {
          // Open shop script button
          let openSSButton = document.getElementById('bk_open_ss')
          openSSButton.style.backgroundColor = 'transparent'
          openSSButton.setAttribute('class', 'bkbtn bkthird')
          openSSButton.disabled = false
          openSSButton.onclick = function () {
            window.open(URL)
          }

          // Open admin hive button
          let openAdminHive = document.getElementById('bk_open_hive')
          openAdminHive.disabled = false
          openAdminHive.style.backgroundColor = 'transparent'
          openAdminHive.setAttribute('class', 'bkbtn bkthird')
          openAdminHive.onclick = function () {
            window.open(hiveURL + shopID)
          }

          // Coppy shop id
          let coppyShopID = document.getElementById('bk_coppy_shop_id')
          coppyShopID.disabled = false
          coppyShopID.style.backgroundColor = 'transparent'
          coppyShopID.setAttribute('class', 'bkbtn bkthird')
          coppyShopID.onclick = function () {
            copyToClipboard(shopID)
            alert('coppied')
          }
        }

        setTimeout(setOnclick, 2000)
      }

      if (URL.includes('shop_id=')) {
      }
      // console.log('global handler', this.responseText)
      // add your global handler here
    })
    return send.apply(this, arguments)
  }
})();

// catch fetch request
(function (ns, fetch) {
  if (typeof fetch !== 'function') return

  ns.fetch = function () {
    var out = fetch.apply(this, arguments)
    if (arguments && arguments[0]) {
      const urlParams = new URLSearchParams(arguments[0])
      if (!hasShopId) {
        const shopId = urlParams.get('shop_id')
        if (shopId) {
          hasShopId = true
          // Got shop id now, assign copy to button
          let coppyShopID = document.getElementById('bk_coppy_shop_id')
          coppyShopID.disabled = false
          coppyShopID.style.backgroundColor = 'transparent'
          coppyShopID.setAttribute('class', 'bkbtn bkthird')
          coppyShopID.onclick = function () {
            copyToClipboard(shopId)
            alert('copied')
          }
        }
      }

      if (!hasAppshopId) {
        const appshopId = urlParams.get('app_shop_id')
        if (appshopId) {
          hasAppshopId = true
          let coppyShopID = document.getElementById('bk_copy_appshop_id')
          coppyShopID.disabled = false
          coppyShopID.style.backgroundColor = 'transparent'
          coppyShopID.setAttribute('class', 'bkbtn bkthird')
          coppyShopID.onclick = function () {
            copyToClipboard(appshopId)
            alert('copied')
          }
        }
      }

      if (!hasUserId) {
        const userId = urlParams.get('user_id')
        if (userId) {
          hasUserId = true
          let coppyShopID = document.getElementById('bk_copy_user_id')
          coppyShopID.disabled = false
          coppyShopID.style.backgroundColor = 'transparent'
          coppyShopID.setAttribute('class', 'bkbtn bkthird')
          coppyShopID.onclick = function () {
            copyToClipboard(userId)
            alert('copied')
          }
        }
      }
      return out
    }

  }

}(window, window.fetch))

