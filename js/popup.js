(function (ns) {
  let fetch = ns.fetch
  if (typeof fetch !== 'function') return

  ns.fetch = function () {
    var out = fetch.apply(this, arguments)
    out.then(function (data) {
      return data.json()
    }).then(function (data) {
      console.log(">>>>", data)
    })

    return out
  }

}(window))
