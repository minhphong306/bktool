'use strict'
var adminHiveURL = ''
var isBktoolOpen = true
var platform = ''
const addIframe = async () => {

}

const addDebugBar = () => {
  // Add debug bar
  const debugBar = document.createElement('div')
  debugBar.setAttribute('id', 'beeketing-debug-sidebar')
  debugBar.setAttribute('class', 'debug-sidebar')
  document.body.appendChild(debugBar)

  // Add hide bar button
  const hideButton = document.createElement('button')
  hideButton.innerHTML = 'Hide bar'
  hideButton.id = 'bk_hide_bar'
  hideButton.setAttribute('class', 'bkbtn bkthird')
  debugBar.appendChild(hideButton)
  hideButton.onclick = function () {
    if (isBktoolOpen) {
      isBktoolOpen = false
      document.getElementById('beeketing-debug-sidebar').style.height = '100px'
    } else {
      isBktoolOpen = true
      document.getElementById('beeketing-debug-sidebar').style.height = '100%'
    }
  }

  // Add reload shop script button
  const reloadButton = document.createElement('button')
  reloadButton.innerHTML = 'Reload Shop Script'
  reloadButton.setAttribute('class', 'bkbtn bkthird')
  debugBar.appendChild(reloadButton)
  reloadButton.onclick = function () {
    window.location.href = appendParamsToURL('reload_shop_script=1')
    location.reload()
  }

  // Add #dev Button
  const devButton = document.createElement('button')
  devButton.innerHTML = 'Add dev param'
  devButton.setAttribute('class', 'bkbtn bkthird')
  debugBar.appendChild(devButton)
  devButton.onclick = function () {
    if (!window.location.href.includes('#dev')) {
      window.location.href += '#dev'
    }
    location.reload()
  }

  // Add open hive admin button
  const hiveButton = document.createElement('button')
  hiveButton.innerHTML = 'Open Hive Admin'
  hiveButton.id = 'bk_open_hive'
  hiveButton.disabled = true
  hiveButton.setAttribute('class', 'bkbtn bkdisabled')
  debugBar.appendChild(hiveButton)

  // Add open shopscript Button
  const openSS = document.createElement('button')
  openSS.innerHTML = 'Open shop script'
  openSS.style.backgroundColor = 'red'
  openSS.disabled = true
  openSS.id = 'bk_open_ss'
  openSS.setAttribute('class', 'bkbtn bkdisabled')
  debugBar.appendChild(openSS)

  // Coppy shop id
  const coppyShopID = document.createElement('button')
  coppyShopID.innerHTML = 'Copy shopID'
  coppyShopID.style.backgroundColor = 'red'
  coppyShopID.disabled = true
  coppyShopID.id = 'bk_coppy_shop_id'
  coppyShopID.setAttribute('class', 'bkbtn bkdisabled')
  debugBar.appendChild(coppyShopID)

  // Check if is in dashboard -> append button get appshop_id, user_id
  if (platform == 'staging_dashboard' || platform == 'production_dashboard') {
    // Coppy appshop id
    const copyAppshop = document.createElement('button')
    copyAppshop.innerHTML = 'Copy appshop ID'
    copyAppshop.style.backgroundColor = 'red'
    copyAppshop.disabled = true
    copyAppshop.id = 'bk_copy_appshop_id'
    copyAppshop.setAttribute('class', 'bkbtn bkdisabled')
    debugBar.appendChild(copyAppshop)

    // Copy user id
    const copyUser = document.createElement('button')
    copyUser.innerHTML = 'Copy user ID'
    copyUser.style.backgroundColor = 'red'
    copyUser.disabled = true
    copyUser.id = 'bk_copy_user_id'
    copyUser.setAttribute('class', 'bkbtn bkdisabled')
    debugBar.appendChild(copyUser)
  }

}

function appendParamsToURL (param) {
  var url = window.location.href
  if (url.indexOf('?') !== -1) {
    if (url.indexOf(param) == -1) {
      url += '&' + param
    }

  } else {
    url += '?' + param
  }
  return url
}

const initContent = () => {
  // Check if content is  loaded
  //detect if is page using beeketing or not
  detectBeeketing(addDebugBar)
}

const detectBeeketing = function (trueCallBack, falseCallBack) {
  let url = window.location.href
  let regex = /https:.+?beeketing.js/

  let matchData = document.body.textContent.match(regex)

  let matchStaging = url.includes('stag.beeketing.com')
  let matchProd = url.includes('fly.beeketing.com')
  let matchDashboard = url.includes('stag.beeketing.com') || url.includes('fly.beeketing.com')

  if (matchData || matchDashboard) {
    utils.listenAjaxComplete()
    if (matchStaging) {
      platform = 'staging_dashboard'
    } else if (matchProd) {
      platform = 'production_dashboard'
    } else if (matchData[0].includes('staging')) {
      platform = 'staging'
      console.log('Staging shop!')
      adminHiveURL = 'https://staging-hive.beeketing.com/admin/view-shop/'
    } else {
      platform = 'production'
      console.log('Production shop!')
      adminHiveURL = 'https://hive.beeketing.com/admin/view-shop/'
    }

    // Create input to write platform
    var platformHiddenInput = document.createElement('input')
    platformHiddenInput.hidden = true
    platformHiddenInput.id = 'bk_platform'
    platformHiddenInput.value = platform

    document.body.appendChild(platformHiddenInput)

    if (trueCallBack) {
      trueCallBack()
    }
  } else {
    if (falseCallBack) {
      falseCallBack()
    }
  }
}

console.log('Start bk tool!!!!!')
initContent()
// # sourceMappingURL = content.js.map

