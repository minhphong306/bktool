const storage = {
  get: e => (
    e = 'bktool-' + e,
      new Promise((t, o) => {
        try {
          chrome.storage.local.get(e, function (o) {t(o[e])})
        } catch (e) {
          o(e)
        }
      })),
  set: (e, t) => {
    let o = {}
    o[e = 'bktool-' + e] = t, chrome.storage.local.set(o)
  }
}
//# sourceMappingURL=storage.js.map
