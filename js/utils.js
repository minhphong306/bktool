const utils = {
  listenAjaxComplete: () => {
    var s = document.createElement('script')
// TODO: add "script.js" to web_accessible_resources in manifest.json
    s.src = chrome.extension.getURL('js/tuto.js')
    s.onload = function () {
      this.remove()
    };
    (document.head || document.documentElement).appendChild(s)
  }, copyToClipboard: (str) => {
    const el = document.createElement('textarea')
    el.value = str
    el.setAttribute('readonly', '')
    el.style.position = 'absolute'
    el.style.left = '-9999px'
    document.body.appendChild(el)
    el.select()
    document.execCommand('copy')
    document.body.removeChild(el)
  }
}
